class student{
    public name:any;
    public surname:string;
    public grade:number;
    constructor(surname:string,name:any,grade:number){
        this.surname = surname;
        this.name = name;
        this.grade = grade;
    }
}

let list1Lenght = 0; 
let list2Lenght = 0; 
let listaStudenti_Promovati:Array<student> = [];
let listaStudenti_Restanti:Array<student> = [];

let btn = document.querySelector('button');

btn.onclick = ()=>{
    addStudent();
    showStudents();
};


function addStudent(){
    let name = (<HTMLInputElement>document.getElementById('name')).value;
    let surname = (<HTMLInputElement>document.getElementById('surname')).value;
    let grade = Number((<HTMLInputElement>document.getElementById('grade')).value);
    if(grade>=5)
    {
        listaStudenti_Promovati[list1Lenght] = new student(surname,name,grade);
        list1Lenght++;
    }
    else
    {
        listaStudenti_Restanti[list2Lenght] = new student(surname,name,grade);
        list2Lenght++;
    } 
}
function showStudents(){
    let table=
    `<tr>
        <th>Nume</th>
        <th>Prenume</th>
        <th>Nota</th>
    </tr>`;
    for(let i=0;i<list1Lenght;i++)
    {
        table += 
        `<tr>
            <td>${listaStudenti_Promovati[i].name}</td>
            <td>${listaStudenti_Promovati[i].surname}</td>
            <td>${listaStudenti_Promovati[i].grade}</td>
        </tr>
        `;
    }
    document.querySelector(".promovati").innerHTML = table;
    table=
    `<tr>
        <th>Nume</th>
        <th>Prenume</th>
        <th>Nota</th>
    </tr>`;
    for(let i=0;i<list2Lenght;i++)
    {
        table += 
        `<tr>
            <td>${listaStudenti_Restanti[i].name}</td>
            <td>${listaStudenti_Restanti[i].surname}</td>
            <td>${listaStudenti_Restanti[i].grade}</td>
        </tr>
        `;
    }
    document.querySelector(".restanti").innerHTML = table;
}