var student = /** @class */ (function () {
    function student(surname, name, grade) {
        this.surname = surname;
        this.name = name;
        this.grade = grade;
    }
    return student;
}());
var list1Lenght = 0;
var list2Lenght = 0;
var listaStudenti_Promovati = [];
var listaStudenti_Restanti = [];
var btn = document.querySelector('button');
btn.onclick = function () {
    addStudent();
    showStudents();
};
function addStudent() {
    var name = document.getElementById('name').value;
    var surname = document.getElementById('surname').value;
    var grade = Number(document.getElementById('grade').value);
    if (grade >= 5) {
        listaStudenti_Promovati[list1Lenght] = new student(surname, name, grade);
        list1Lenght++;
    }
    else {
        listaStudenti_Restanti[list2Lenght] = new student(surname, name, grade);
        list2Lenght++;
    }
}
function showStudents() {
    var table = "<tr>\n        <th>Nume</th>\n        <th>Prenume</th>\n        <th>Nota</th>\n    </tr>";
    for (var i = 0; i < list1Lenght; i++) {
        table +=
            "<tr>\n            <td>" + listaStudenti_Promovati[i].name + "</td>\n            <td>" + listaStudenti_Promovati[i].surname + "</td>\n            <td>" + listaStudenti_Promovati[i].grade + "</td>\n        </tr>\n        ";
    }
    document.querySelector(".promovati").innerHTML = table;
    table =
        "<tr>\n        <th>Nume</th>\n        <th>Prenume</th>\n        <th>Nota</th>\n    </tr>";
    for (var i = 0; i < list2Lenght; i++) {
        table +=
            "<tr>\n            <td>" + listaStudenti_Restanti[i].name + "</td>\n            <td>" + listaStudenti_Restanti[i].surname + "</td>\n            <td>" + listaStudenti_Restanti[i].grade + "</td>\n        </tr>\n        ";
    }
    document.querySelector(".restanti").innerHTML = table;
}
